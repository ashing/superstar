// author: ashing
// time: 2020/2/20 8:38 下午
// mail: axingfly@gmail.com
// Less is more.

package main

import (
	"github.com/kataras/iris/v12"
)

func main() {
	app := iris.New()

	h := iris.HTML("_demo/iris", ".html")
	app.RegisterView(h)

	app.Get("/", func(ctx iris.Context) {
		ctx.WriteString("hello world from iris")
	})

	app.Get("/hello", func(ctx iris.Context) {
		ctx.ViewData("Title", "iris")
		ctx.ViewData("Content", "hello world")
		ctx.View("hello.html")
	})

	app.Run(iris.Addr("127.0.0.1:8080"), iris.WithCharset("UTF-8"))

}
